package model;

import org.cyberborean.rdfbeans.annotations.RDF;
import org.cyberborean.rdfbeans.annotations.RDFBean;
import org.cyberborean.rdfbeans.annotations.RDFNamespaces;
import org.cyberborean.rdfbeans.annotations.RDFSubject;

@RDFNamespaces({
        "foaf = http://xmlns.com/foaf/0.1/",
        "ex = http://example.org/"
})
@RDFBean("foaf:Person")
public class Person {
    private String id;
    private String name;
    private int age;

    public Person() {
    }

    @RDFSubject(prefix = "http://example.org/persons/")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @RDF("foaf:name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @RDF("foaf:age")
    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
