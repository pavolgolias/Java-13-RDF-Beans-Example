package model;

import org.cyberborean.rdfbeans.annotations.RDF;
import org.cyberborean.rdfbeans.annotations.RDFBean;
import org.cyberborean.rdfbeans.annotations.RDFNamespaces;

@RDFNamespaces({
        "foaf = http://xmlns.com/foaf/0.1/",
        "persons = http://rdfbeans.viceversatech.com/test-ontology/persons/"
})
@RDFBean("foaf:PersonOld")
public class PersonOldAddition extends PersonOld {
    private String phone;

    public PersonOldAddition() {
    }

    @RDF("foaf:phone")
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "PersonOldAddition{" +
                "phone='" + phone + '\'' +
                "} " + super.toString();
    }
}
