﻿## RDF Beans usage example
> Small project which shows an example how to use RDF Beans - easy framework which enables mapping between RDF tripple store and Java POJO objects.
 
 **RDF Beans:**
 - [RDFBeans framework](https://github.com/cyberborean/rdfbeans)
 - [RDFBeans framework (forked working repository)](https://github.com/pavolgolias/rdfbeans)
 
 > For usage of RDFBeans add correct dependencies to your POM file or build the project from repository mentioned above and install it to your local maven repository.  
