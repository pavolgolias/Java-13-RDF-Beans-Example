import model.Person;
import model.PersonOld;
import model.PersonOldAddition;
import org.cyberborean.rdfbeans.RDFBeanManager;
import org.cyberborean.rdfbeans.exceptions.RDFBeanException;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.query.QueryResults;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryResult;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.sail.memory.MemoryStore;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Application {
    public static void main(String[] args) throws RDFBeanException {
        //String rdf4jServer = "http://localhost:8080/rdf4j-server/";
        //String repositoryID = "first-repo";
        //Repository repository = new HTTPRepository(rdf4jServer, repositoryID);

        Repository repository = new SailRepository(new MemoryStore());
        repository.initialize();

        //Repository repository = new SailRepository(new MemoryStore());
        //repository.initialize();
       /* RepositoryModel model = new RepositoryModel(repository);
        model.open();*/

        RDFBeanManager manager = new RDFBeanManager(repository.getConnection());

        Resource r = manager.getResource("esmall", PersonOld.class);
        System.out.println(r);

        PersonOldAddition person = new PersonOldAddition();
        person.setId("http://example.com/persons/esmalll");
        person.setName("Emily smalll");
        person.setPhone("0915");

        PersonOldAddition person2 = new PersonOldAddition();
        person2.setId("http://example.com/persons/key1");
        person2.setName("person 2");
        person2.setPhone("0915");

        List<PersonOld> knowsList = new ArrayList<PersonOld>();
        knowsList.add(person);
        person2.setKnows(knowsList);

        r = manager.add(person);
        Resource r2 = manager.add(person2);

        PersonOldAddition loadedPerson = manager.get(r, PersonOldAddition.class);
        PersonOldAddition loadedPerson2 = manager.get(r2, PersonOldAddition.class);
        System.out.println(loadedPerson);
        System.out.println(loadedPerson2);

        System.out.println(loadedPerson.getName());
        System.out.println(loadedPerson.getPhone());

        List<String> tmp = new ArrayList<>();
        tmp.add("Swimming");
        tmp.add("Skiing");

        Person newPerson = new Person();
        newPerson.setId("1");
        newPerson.setName("Name Surname");
        newPerson.setAge(25);

        manager.add(newPerson);
        System.out.println();
        System.out.println(manager.get("1", Person.class));

        try (RepositoryConnection connection = repository.getConnection()) {
            IRI id = connection.getValueFactory().createIRI("http://example.org/persons/1");
            RepositoryResult<Statement> statements = connection.getStatements(id, null, null);
            Model model = QueryResults.asModel(statements);

            System.out.println("\n\nN-TRIPLES:");
            Rio.write(model, System.out, RDFFormat.NTRIPLES);
            System.out.println("\n\nTURTLE:");
            Rio.write(model, System.out, RDFFormat.TURTLE);
            System.out.println("\n\nRDF/XML:");
            Rio.write(model, System.out, RDFFormat.RDFXML);
            System.out.println("\n\nRDF/JSON:");
            Rio.write(model, System.out, RDFFormat.RDFJSON);
        }
    }
}
