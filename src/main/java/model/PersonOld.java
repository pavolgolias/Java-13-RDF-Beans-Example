package model;

import org.cyberborean.rdfbeans.annotations.*;

import java.net.URI;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@RDFNamespaces({
        "foaf = http://xmlns.com/foaf/0.1/",
        "persons = http://rdfbeans.viceversatech.com/test-ontology/persons/"
})
@RDFBean("foaf:PersonOld")
public class PersonOld {

    private String id;
    private String name;
    private String email;
    private URI homepage;
    private Date birthday;
    private String[] nick;
    private List<PersonOld> knows;

    /** Default no-arg constructor */
    public PersonOld() {
    }

    @RDFSubject(prefix = "persons:")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @RDF("foaf:name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @RDF("foaf:mbox")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @RDF("foaf:homepage")
    public URI getHomepage() {
        return homepage;
    }

    public void setHomepage(URI homepage) {
        this.homepage = homepage;
    }

    @RDF("foaf:birthday")
    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    @RDF("foaf:nick")
    @RDFContainer(RDFContainer.ContainerType.ALT)
    public String[] getNick() {
        return nick;
    }

    public void setNick(String[] nick) {
        this.nick = nick;
    }

    public String getNick(int i) {
        return nick[i];
    }

    public void setNick(int i, String nick) {
        this.nick[i] = nick;
    }

    @RDF("foaf:knows")
    @RDFContainer(RDFContainer.ContainerType.SEQ)
    public List<PersonOld> getKnows() {
        return knows;
    }

    public void setKnows(List<PersonOld> knows) {
        this.knows = knows;
    }

    @Override
    public String toString() {
        return "PersonOld{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", homepage=" + homepage +
                ", birthday=" + birthday +
                ", nick=" + Arrays.toString(nick) +
                ", knows=" + (knows != null) +
                '}';
    }
}
